FROM docker.io/library/alpine:3.17.2
ARG BUILD_VERSION
LABEL org.opencontainers.image.authors="agafenco@gmail.com"
LABEL org.opencontainers.image.version=$BUILD_VERSION
LABEL org.opencontainers.image.vendor="AGA Systems"
LABEL org.opencontainers.image.description="Simple Payroll service"
RUN apk --no-cache --update add libc6-compat=1.2.3-r4 mariadb-dev=10.6.12-r0 tzdata=2022f-r1; \
    addgroup -S app && adduser -S -G app app;
COPY ./build/payrollService /opt/app/payrollService
USER app:app
WORKDIR /opt/app
ENTRYPOINT ["./payrollService"]

unit uAppLog;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, syncobjs, hAppLog;

type

  { TAppLog }
  TAppLog = class(TObject)
  private
    FLogLevel: TLogLevel;
    FLock: TCriticalSection;
    FIdentification: string;
    FTimeStampFormat: string;
  private
    procedure SetLogLevel(ALogLevel: TLogLevel);
    procedure SetIdentification(AIdentification: string);
    procedure SetTimeStampFormat(ATimeStampFormat: string);
    function FormatLogMessage(ALogLevel: TLogLevel; const Msg: string): string;
  public
    constructor Create(ALogLevel: TLogLevel; AIdentification, ATimeStampFormat: string);
    destructor Destroy; override;
    procedure AddLog(ALogLevel: TLogLevel; AMsg: string);
  published
    property LogLevel: TLogLevel read FLogLevel write SetLogLevel;
    property Identification: string read FIdentification write SetIdentification;
    property TimeStampFormat: string read FTimeStampFormat write SetTimeStampFormat;
  end;


implementation

uses
  uMain;

{ TAppLog }

procedure TAppLog.SetLogLevel(ALogLevel: TLogLevel);
begin
  FLock.Enter;
  try
    FLogLevel := ALogLevel;
  finally
    FLock.Leave();
  end;
end;

procedure TAppLog.SetIdentification(AIdentification: string);
begin
  FLock.Enter;
  try
    FIdentification := AIdentification;
  finally
    FLock.Leave();
  end;
end;

procedure TAppLog.SetTimeStampFormat(ATimeStampFormat: string);
begin
  FLock.Enter;
  try
    FTimeStampFormat := ATimeStampFormat;
  finally
    FLock.Leave();
  end;
end;

function TAppLog.FormatLogMessage(ALogLevel: TLogLevel; const Msg: string): string;
var
  TS, T: string;
begin
  if FTimeStampFormat = '' then
    FTimeStampFormat := 'yyyy-mm-dd hh:nn:ss.zzz';
  TS := FormatDateTime(FTimeStampFormat, Now);
  T := LogLevelToString(ALogLevel);
  Result := Format('%s [%s %s] %s%s', [FIdentification, TS, T, Msg, LineEnding]);
end;

procedure TAppLog.AddLog(ALogLevel: TLogLevel; AMsg: string);
begin
  if ALogLevel <= FLogLevel then
    WriteToStdout(FormatLogMessage(ALogLevel, AMsg));
end;

constructor TAppLog.Create(ALogLevel: TLogLevel;
  AIdentification, ATimeStampFormat: string);
begin
  inherited Create();
  FIdentification := AIdentification;
  FLock := TCriticalSection.Create();
  FLogLevel := ALogLevel;
  FTimeStampFormat := ATimeStampFormat;
end;

destructor TAppLog.Destroy;
begin
  FreeAndNil(FLock);
  inherited Destroy();
end;

end.

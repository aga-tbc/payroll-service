unit hAppLog;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, baseunix, fpjson, unix;

const
  SLogTypeAccess: string = 'access';
  SLogTypeSecurity: string = 'security';
  SLogTypeApplication: string = 'application';

  SLogLevelError: string = 'ERROR';
  SLogLevelWarning: string = 'WARNING';
  SLogLevelInfo: string = 'INFO';
  SLogLevelDebug: string = 'DEBUG';

type
  //TLogFormatType = (lfText, ltJson);
  TLogLevel = (llError, llWarning, llInfo, llDebug);
  TLogRecordType = (rtAccess, rtSecurity, rtApplication);



procedure WriteToStdOut(const S: string);
function LogTimeStampToString(ATimeStamp: TDateTime;
  ATimeStampFormat: string = ''): string;
function LogTimeStampToString(ATimeStampFormat: string = ''): string;
function LogLevelToString(ALogLevel: TLogLevel): string;


implementation

function LogTimeStampToString(ATimeStamp: TDateTime;
  ATimeStampFormat: string = ''): string;
begin
  if ATimeStampFormat = '' then
    ATimeStampFormat := 'yyyy-mm-ddThh:nn:ss';
  Result := FormatDateTime(ATimeStampFormat, ATimeStamp);
end;

function LogTimeStampToString(ATimeStampFormat: string = ''): string;
begin
  Result := LogTimeStampToString(Now, ATimeStampFormat);
end;


function LogLevelToString(ALogLevel: TLogLevel): string;
begin
  Result := SLogLevelInfo;
  case ALogLevel of
    llError: Result := SLogLevelError;
    llWarning: Result := SLogLevelWarning;
    llInfo: Result := SLogLevelInfo;
    llDebug: Result := SLogLevelDebug;
    else
      raise Exception.Create('Invalid Log Level value.');
  end;
end;

procedure WriteToStdOut(const S: string);
begin
  //Writeln(S);
  FpWrite(0, PChar(S), length(S));
end;



end.

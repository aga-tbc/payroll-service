unit uAppSettingsTest;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, hAppSettings,
  uAppSettings, hAppLog;

type

  TAppSettingsTest = class(TTestCase)
  private
    FAppSettings: TAppSettings;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestHookUp;
  end;

implementation

procedure TAppSettingsTest.TestHookUp;
//var
//  I: integer;
begin
  //for I := 0 to getEnvironmentVariableCount() - 1 do
    //writeLn('export ' + getEnvironmentString(I));
  AssertEquals('', GetEnvStringValue('TEST_UNKNOWN_STRING_ENV_VAR', True));
  AssertEquals('6Q9YiWesjLzKsbYNet', GetEnvStringValue('TEST_STRING_ENV_VAR'));
  AssertEquals(True, GetEnvBoolValue('TEST_UNKNOWN_BOOL_ENV_VAR', True, True));
  FAppSettings.LoadSettings();
  with FAppSettings.LockAppSettngs do
    try
      AssertEquals(Logging.Identification, 'payrollService');
      AssertEquals(Integer(Logging.LogLevel), Integer(llInfo));
      AssertEquals(Logging.TimeStampFormat, '');

      AssertEquals(DbConnection.Protocol, 'MariaDB-10');
      AssertEquals(DbConnection.HostName, 'localhost');
      AssertEquals(DbConnection.Port, 3306);
      AssertEquals(DbConnection.User, 'payroll');
      AssertEquals(DbConnection.Password, 'thepassword');
      AssertEquals(DbConnection.Database, 'payroll');
      AssertEquals(ApiCredencials[0], 'NlE5WWlXZXNqTHpLc2JZTmV0Cg==');
    finally
      FAppSettings.UnlockAppSettings();
    end;
end;

procedure TAppSettingsTest.SetUp;
begin
  FAppSettings := TAppSettings.Create();
end;

procedure TAppSettingsTest.TearDown;
begin
  FreeAndNil(FAppSettings);
end;

initialization

  RegisterTest(TAppSettingsTest);
end.

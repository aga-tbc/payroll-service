{$mode objfpc}
{$H+}
unit uMain;

interface

uses baseunix, SysUtils, Classes, custhttpapp, httpdefs, httproute, fpjson,
  fileinfo, uAppSettings, uAppLog, hAppLog, uCommon, uEmployeeDM,
  strutils{, base64};

type

  { THTTPApplication }

  THTTPApplication = class(TCustomHTTPApplication)
  private
    FAppVersion: string;
    FAppName: string;
    FAppSettings: TAppSettings;
    FAppLog: TAppLog;

  private
    function Authenticate(ARequest: TRequest; AResponse: TResponse): boolean;
    procedure statusEndpoint(Req: TRequest; Res: TResponse);
    procedure handleEmployeeByIdRequest(Req: TRequest; Res: TResponse);
    procedure handleEmployeeRequest(Req: TRequest; Res: TResponse);
    procedure ExceptionHandler(E: Exception; Res: TResponse; AProcName: string);
  protected

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property AppSettings: TAppSettings read FAppSettings;
    property AppLog: TAppLog read FAppLog;
    property AppVersion: string read FAppVersion;
  end;

var
  Application: THTTPApplication;
  ShowCleanUpErrors: boolean = False;

implementation

uses CustApp;

{ THTTPApplication }

function THTTPApplication.Authenticate(ARequest: TRequest;
  AResponse: TResponse): boolean;
var
  headerValue, ACredentials{, b64decoded, username, password}: string;
  AIndex: integer;
begin
  Result := False;
  try
    headerValue := aRequest.Authorization;

    if length(headerValue) = 0 then
    begin
      jsonResponse(AResponse, 401, GetErrorMessage(ecAuthRequired,
        'This endpoint requires authentication'));
      Exit;
    end;

    if ExtractWord(1, headerValue, [' ']) <> 'Basic' then
    begin
      jsonResponse(AResponse, 401, GetErrorMessage(ecAuthRequired,
        'Only Basic Authentication is supported'));
      Exit;
    end;
    ACredentials := ExtractWord(2, headerValue, [' ']);
  {b64decoded := DecodeStringBase64(ExtractWord(2, headerValue, [' ']));
  username := ExtractWord(1, b64decoded, [':']);
  password := ExtractWord(2, b64decoded, [':']);}
    with AppSettings.LockAppSettngs do
      try
        Result := ApiCredencials.Find(ACredentials, AIndex);
      finally
        AppSettings.UnlockAppSettings();
      end;
    if not Result then
      jsonResponse(AResponse, 401, GetErrorMessage(ecAuthRequired,
        'Invalid API credentials'));
  except
    on E: Exception do
      jsonResponse(AResponse, 500, GetErrorMessage(ecUnknownError,
        'Unexpected server error.'), 'Authenticate: ' + E.Message);
  end;
end;

procedure THTTPApplication.statusEndpoint(Req: TRequest; Res: TResponse);
var
  jObject: TJSONObject;
begin
  jObject := TJSONObject.Create;
  try
    if Req.Method = 'GET' then
    begin
      jObject.Add('name', FAppName);
      jObject.Add('status', 'ok');
      jObject.Add('version', FAppVersion);
      jsonResponse(Res, 200, jObject.AsJSON);
    end
    else
      jsonResponse(Res, 405, GetErrorMessage(ecInvalidHttpMethod,
        'Invalid HTTP method.'));
  finally
    jObject.Free;
  end;
end;

procedure THTTPApplication.handleEmployeeByIdRequest(Req: TRequest; Res: TResponse);
var
  AEmployee: TEmployee;
begin
  if not Authenticate(Req, Res) then
    Exit;
  try
    AEmployee := TEmployee.Create();
    try
      case Req.Method of
        'GET':
          jsonResponse(Res, 200, AEmployee.GetEmployeeById(
            req.RouteParams['employeeId']));
        'PATCH':
        begin
          AEmployee.UpdateEmployee(Req);
          jsonResponse(Res, 204, '');
        end;
        'DELETE':
        begin
          AEmployee.DeleteEmployee(req.RouteParams['employeeId']);
          jsonResponse(Res, 204, '');
        end
        else
          jsonResponse(Res, 405, GetErrorMessage(ecInvalidHttpMethod,
            'Invalid HTTP method for this endpoint.'));
      end;
    finally
      AEmployee.Free();
    end;
  except
    on E: Exception do
      ExceptionHandler(E, Res, 'handleEmployeesByIdRequest');
  end;
end;

procedure THTTPApplication.handleEmployeeRequest(Req: TRequest; Res: TResponse);
var
  AEmployee: TEmployee;
begin
  if not Authenticate(Req, Res) then
    Exit;
  try
    AEmployee := TEmployee.Create();
    try
      case Req.Method of
        'GET':
          jsonResponse(Res, 200, AEmployee.GetEmployees(Req));
        'POST':
          jsonResponse(Res, 201, AEmployee.AddEmployee(Req));
        else
          jsonResponse(Res, 405, GetErrorMessage(ecInvalidHttpMethod,
            'Invalid HTTP method for this endpoint.'));
      end;
    finally
      AEmployee.Free();
    end;
  except
    on E: Exception do
      ExceptionHandler(E, Res, 'handleEmployeeRequest');
  end;
end;

procedure THTTPApplication.ExceptionHandler(E: Exception; Res: TResponse;
  AProcName: string);
begin
  try
    //WriteToStdOut('Exception: ' + E.ClassName + LineEnding);
    case E.ClassName of
      'EClientException':
        jsonResponse(Res, 400, GetErrorMessage(ecClientError, E.Message),
          AProcName + ': ' + E.Message);
      'EJSONParser':
        jsonResponse(Res, 400, GetErrorMessage(ecJsonParsing, E.Message),
          AProcName + ': ' + E.Message);
      'ENoDataFoundException': jsonResponse(Res, 404,
          GetErrorMessage(ecNoDataFound, E.Message),
          AProcName + ': ' + E.Message)
      else
        jsonResponse(Res, 500, GetErrorMessage(ecUnknownError, E.Message),
          AProcName + ': ' + E.Message);
    end;
  except
    on E: Exception do
      AppLog.AddLog(llError, 'ExceptionHandler: ' + E.Message);
  end;
end;

constructor THTTPApplication.Create(AOwner: TComponent);
var
  FileVerInfo: TFileVersionInfo;
begin
  inherited Create(AOwner);
  FileVerInfo := TFileVersionInfo.Create(nil);
  try
    FileVerInfo.ReadFileInfo;
    FAppVersion := FileVerInfo.VersionStrings.Values['FileVersion'];
    FAppName := FileVerInfo.VersionStrings.Values['ProductName'];
  finally
    FileVerInfo.Free;
  end;
  FAppSettings := TAppSettings.Create();
  with FAppSettings.LockAppSettngs() do
    try
      FAppLog := TAppLog.Create(llInfo, FAppName, '');
    finally
      FAppSettings.UnlockAppSettings();
    end;
end;

destructor THTTPApplication.Destroy;
begin
  FreeAndNil(FAppSettings);
  FreeAndNil(FAppLog);
  inherited Destroy;
end;


procedure SignalHandler(signal: cint); cdecl;
begin
  Application.AppLog.AddLog(llInfo, Format('Signal %d received. Exiting...', [signal]));
  Application.Terminate();
  Halt(0);
end;

procedure InitHTTP;
//var
//I: integer;
begin
  //for I := 0 to getEnvironmentVariableCount() - 1 do
  //writeLn('export ' + getEnvironmentString(I));
  RegisterSignalsHandler([SIGTERM, SIGINT], @SignalHandler);
  Application := THTTPApplication.Create(nil);
  if not assigned(CustomApplication) then
    CustomApplication := Application;

  if Application.HasOption('v', 'version') then
  begin
    WriteToStdOut(Application.AppVersion {+ LineEnding});
    Application.Terminate();
    Halt(0);
  end;

  Application.AppSettings.LoadSettings();
  Application.Port := 8080;
  with Application.AppSettings.LockAppSettngs do
    try
      Application.AppLog.LogLevel := Logging.LogLevel;
      Application.AppLog.Identification := Logging.Identification;
      Application.AppLog.TimeStampFormat := Logging.TimeStampFormat;
    finally
      Application.AppSettings.UnlockAppSettings;
    end;

  HTTPRouter.RegisterRoute('/api/v1/status', @Application.statusEndpoint, True);
  HTTPRouter.RegisterRoute('/api/v1/employee/', @Application.handleEmployeeRequest);
  HTTPRouter.RegisterRoute('/api/v1/employee/:employeeId',
    @Application.handleEmployeeByIdRequest);

  Application.AppLog.AddLog(llInfo,
    Format('Application starting, http port: %d', [Application.Port]));
end;

procedure DoneHTTP;
begin
  if CustomApplication = Application then
    CustomApplication := nil;
  try
    FreeAndNil(Application);
  except
    if ShowCleanUpErrors then
      raise;
  end;
end;



initialization
  InitHTTP;

finalization
  DoneHTTP;

end.

unit hEmployeeDM;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils;

const
  dfnId = 'id';
  dfnEmployeeNo = 'employee_no';
  dfnFirstName = 'firstname';
  dfnMiddleName = 'middlename';
  dfnLastName = 'lastname';
  dfnName = 'name';
  dfnDepartmentId = 'department_id';
  dfnDepartmentName = 'department_name';
  dfnPositionId = 'position_id';
  dfnPositionName = 'position_name';
  dfnSalary = 'salary';

  pfnId = 'id';
  pfnEmployeeId = 'employeeId';
  pfnName = 'name';
  pfnEmployeeNo = 'employeeNo';
  pfnFirstName = 'firstName';
  pfnMiddleName = 'middleName';
  pfnLastName = 'lastName';
  pfnDepartment = 'department';
  pfnDepartmentId = 'departmentId';
  pfnDepartmentName = 'departmentName';
  pfnPosition = 'position';
  pfnPositionId = 'positionId';
  pfnPositionName = 'positionName';
  pfnSalary = 'salary';

implementation

end.

unit uCommon;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, syncobjs, baseunix, HTTPDefs, fpjson;

const
  //ErrorCode
  ecUnknownError = '1000';
  ecJsonParsing = '1001';
  ecClientError = '1002';
  ecAuthRequired = '1003';
  ecInvalidAuthMethod = '1004';
  ecInvlaidCredencials = '1005';
  ecInvalidHttpMethod = '1006';
  ecNoDataFound = '1007';


  SFieldNotFound = 'Field %s not found.';

type
  EClientException = class(Exception);
  ENoDataFoundException = class(Exception);


  { TThreadQueue }

  TThreadSequence = class(TObject)
  private
    FSequence: integer;
    FLock: TCriticalSection;
  public
    function NextSequence: integer;
    constructor Create();
    destructor Destroy; override;
  end;

function GetStringGuid: string;
procedure ZeroMemory(var Destination; Length: SizeInt);
procedure RegisterSignalHandler(signal: cint; SignalHandler: Pointer);
procedure RegisterSignalsHandler(signals: array of cint; SignalHandler: Pointer);
procedure jsonResponse(var ARes: TResponse; ACode: integer; AData: string; AErrorMessage: string = '');
function GetErrorMessage(ACode, AMessage: string): string;

implementation

uses
  uMain, hAppLog;

procedure jsonResponse(var ARes: TResponse; ACode: integer; AData: string; AErrorMessage: string = '');
begin
  try
    if ACode >= 400 then
     Application.AppLog.AddLog(llError, AErrorMessage);
    ARes.Content := AData;
    ARes.Code := ACode;
    ARes.ContentType := 'application/json';
    ARes.ContentLength := Length(ARes.Content);
    ARes.SendContent;
  except
  end;
end;

function GetErrorMessage(ACode, AMessage: string): string;
var
  jObject: TJSONObject;
begin
  Result := '';
  try
    jObject := TJSONObject.Create;
    try
      jObject.Add('code', ACode);
      jObject.Add('message', AMessage);
      Result := jObject.AsJSON;
    finally
      jObject.Free;
    end;
  except
  end;
end;

procedure ZeroMemory(var Destination; Length: SizeInt);
begin
  FillByte(Destination, Length, 0);
end;

function GetStringGuid: string;
var
  AGuid: TGUID;
begin
  if (CreateGUID(AGuid) = 0) then
    Result := GUIDToString(AGuid)
  else
    Result := '';
end;


procedure RegisterSignalHandler(signal: cint; SignalHandler: Pointer);
var
  SigAction: PSigActionRec;
begin
  New(SigAction);
  try
    SigAction^.sa_handler := sigActionHandler(SignalHandler);
    FillChar(SigAction^.sa_mask, sizeof(SigAction^.sa_mask), #0);
    SigAction^.sa_flags := 0;
    fpSigAction(signal, SigAction, nil);
  finally
    Dispose(SigAction);
  end;
end;

procedure RegisterSignalsHandler(signals: array of cint; SignalHandler: Pointer);
var
  I: integer;
begin
  for I := Low(signals) to High(signals) do
    RegisterSignalHandler(signals[I], SignalHandler);
end;

{ TThreadSequence }

function TThreadSequence.NextSequence: integer;
begin
  FLock.Enter();
  try
    try
      Inc(FSequence);
    except
      FSequence := 0;
    end;
    Result := FSequence;
  finally
    FLock.Leave();
  end;
end;

constructor TThreadSequence.Create;
begin
  FSequence := 0;
  FLock := TCriticalSection.Create();
  inherited Create();
end;

destructor TThreadSequence.Destroy;
begin
  FreeAndNil(FLock);
  inherited Destroy();
end;

end.

unit hAppSettings;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, eventlog, hAppLog;

const

  SInvalidEnvVarValue: string = 'Invalid value "%s" for environment variable %s.';
  SInvalidEnvVarFileNameValue: string =
    'File "%s" specified in environment variable %s doesn'' exists.';
  SInvalidEnvVarDirNameValue: string =
    'Directory "%s" specified in environment variable %s doesn'' exists.';
  SInvalidEnvVarNullStringValue: string =
    'Invalid null value for environment variable %s.';
  SInvalidEnvVarMaxLenthValue: string =
    'Invalid length %d for environment variable %s.';


  //Environment Variables
  evLoggingLogLevel = 'LOGGING_LOG_LEVEL';
  evLoggingIdentification = 'LOGGING_IDENTIFICATION';
  evLoggingTimeStampFormat = 'LOGGING_TIMESTAMPFORMAT';

  evDbConnectionProtocol = 'DBCONNECTION_PROTOCOL';
  evDbConnectionHostName = 'DBCONNECTION_HOSTNAME';
  evDbConnectionPort = 'DBCONNECTION_PORT';
  evDbConnectionUser = 'DBCONNECTION_USER';
  evDbConnectionPassword = 'DBCONNECTION_PASSWORD';
  evDbConnectionDatabase = 'DBCONNECTION_DATABASE';
  evDbConnectionMaxconnections = 'DBCONNECTION_MAXCONNECTIONS';
  evDbConnectionWait = 'DBCONNECTION_WAIT';
  evDbConnectionTimeout = 'DBCONNECTION_TIMEOUT';
  evApiCredencials = 'API_CREDENCIALS';


type
  TLoggingObject = class(TObject)
  private
    FLogLevel: TLogLevel;
    FIdentification: string;
    FTimeStampFormat: string;
  published
    property LogLevel: TLogLevel read FLogLevel write FLogLevel;
    property Identification: string read FIdentification write FIdentification;
    property TimeStampFormat: string read FTimeStampFormat write FTimeStampFormat;
  end;

  TDbConnectionObject = class(TObject)
  private
    FProtocol: string;
    FHostName: string;
    FPort: Integer;
    FUser: string;
    FPassword: string;
    FDatabase: string;
    FMaxConnections: Integer;
    FWait: Boolean;
    FTimeout: Integer;
  published
    property Protocol: string read FProtocol write FProtocol;
    property HostName: string read FHostName write FHostName;
    property Port: Integer read FPort write FPort;
    property User: string read FUser write FUser;
    property Password: string read FPassword write FPassword;
    property Database: string read FDatabase write FDatabase;
    property MaxConnections: Integer read FMaxConnections write FMaxConnections;
    property Wait: Boolean read FWait write FWait;
    property Timeout: Integer read FTimeout write FTimeout;
  end;



  { TSettings }

  TSettings = class(TObject)
  private
    FLogging: TLoggingObject;
    FDbConnection: TDbConnectionObject;
    FApiCredencials: TStringList;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property Logging: TLoggingObject read FLogging;
    property DbConnection: TDbConnectionObject read FDbConnection;
    property ApiCredencials: TStringList read FApiCredencials;
  end;

function GetEnvStringValue(EnvVar: string; IsNullable: boolean = False;
  MaxLength: integer = 255): string;
function GetEnvBoolValue(EnvVar: string; IsNullable: boolean = True;
  NullValue: boolean = False): boolean;
function GetEnvIntValue(EnvVar: string; IsNullable: boolean = True;
  NullValue: integer = 0): integer;
function GetEnvLogTypeValue(EnvVar: string; IsNullable: boolean = True): TLogType;
function GetEnvLogLevelValue(EnvVar: string; IsNullable: boolean = True): TLogLevel;

function GetEnvFileNameValue(EnvVar: string;
  CheckFileExists: boolean = False): TFileName;

implementation

function GetEnvStringValue(EnvVar: string; IsNullable: boolean = False;
  MaxLength: integer = 255): string;
var
  L: integer;
begin
  Result := Trim(GetEnvironmentVariable(EnvVar));
  L := Length(Result);
  if (not IsNullable) and (L = 0) then
    raise Exception.CreateFmt(SInvalidEnvVarNullStringValue, [EnvVar]);
  if L > MaxLength then
    raise Exception.CreateFmt(SInvalidEnvVarMaxLenthValue, [L, EnvVar]);
end;

function GetEnvBoolValue(EnvVar: string; IsNullable: boolean = True;
  NullValue: boolean = False): boolean;
var
  S: string;
begin
  S := GetEnvironmentVariable(EnvVar);
  case LowerCase(Trim(S)) of
    'true': Result := True;
    'false': Result := False;
    else
    begin
      if (IsNullable) and (S = '') then
        Result := NullValue
      else
        raise Exception.CreateFmt(SInvalidEnvVarValue, [S, EnvVar]);
    end;
  end;
end;

function GetEnvIntValue(EnvVar: string; IsNullable: boolean = True;
  NullValue: integer = 0): integer;
var
  S: string;
begin
  S := GetEnvironmentVariable(EnvVar);
  if (IsNullable) and (S = '') then
    Result := NullValue
  else
    try
      Result := StrToInt(S);
    except
      raise Exception.CreateFmt(SInvalidEnvVarValue, [S, EnvVar]);
    end;
end;

function GetEnvFileNameValue(EnvVar: string;
  CheckFileExists: boolean = False): TFileName;
begin
  Result := GetEnvironmentVariable(EnvVar);
  if Result = '' then
    Result := ChangeFileExt(ParamStr(0), 'log')
  else
  begin
    if CheckFileExists then
    begin
      if not FileExists(Result) then
        raise Exception.CreateFmt(SInvalidEnvVarFileNameValue, [Result, EnvVar]);
    end
    else if not DirectoryExists(ExtractFileDir(Result)) then
      raise Exception.CreateFmt(SInvalidEnvVarDirNameValue, [Result, EnvVar]);
  end;
end;

function GetEnvLogTypeValue(EnvVar: string; IsNullable: boolean = True): TLogType;
var
  S: string;
begin
  S := GetEnvironmentVariable(EnvVar);
  case LowerCase(Trim(S)) of
    'system': Result := ltSystem;
    'file': Result := ltFile;
    'stdout': Result := ltStdOut;
      //'stderr': Result := ltStdErr;
    else
    begin
      if (IsNullable) and (S = '') then
        Result := ltStdOut
      else
        raise Exception.CreateFmt(SInvalidEnvVarValue, [S, EnvVar]);
    end;
  end;
end;

function GetEnvLogLevelValue(EnvVar: string; IsNullable: boolean = True): TLogLevel;
var
  S: string;
begin
  S := GetEnvironmentVariable(EnvVar);
  case LowerCase(Trim(S)) of
    'error': Result := llError;
    'warn': Result := llWarning;
    'info': Result := llInfo;
    'debug': Result := llDebug;
    else
    begin
      if (IsNullable) and (S = '') then
        Result := llInfo
      else
        raise Exception.CreateFmt(SInvalidEnvVarValue, [S, EnvVar]);
    end
  end;
end;


{ TSettings }

constructor TSettings.Create;
begin
  FLogging := TLoggingObject.Create();
  FDbConnection := TDbConnectionObject.Create();
  FApiCredencials := TStringList.Create();
  FApiCredencials.Sorted := True;
end;

destructor TSettings.Destroy;
begin
  FreeAndNil(FApiCredencials);
  FreeAndNil(FLogging);
  FreeAndNil(FDbConnection);
  inherited Destroy;
end;

end.

unit uAppSettings;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, unix, syncobjs, hAppSettings;

type

  { TAppSettings }

  TAppSettings = class(TObject)
  private
    FLock: TCriticalSection;
    FSettings: TSettings;
  public
    constructor Create();
    destructor Destroy(); override;
    procedure LoadSettings();
    function LockAppSettngs: TSettings;
    procedure UnlockAppSettings;
  end;

//var
//  AppSettings: TAppSettings;

implementation

{ TAppSettings }

constructor TAppSettings.Create();
begin
  inherited Create();
  FLock := TCriticalSection.Create();
  FSettings := TSettings.Create();
end;

destructor TAppSettings.Destroy;
begin
  FSettings.Free();
  FLock.Free();
  inherited Destroy();
end;

procedure TAppSettings.LoadSettings();
begin
  with LockAppSettngs do
    try
      Logging.LogLevel := GetEnvLogLevelValue(evLoggingLogLevel);
      Logging.Identification := GetEnvStringValue(evLoggingIdentification);
      Logging.TimeStampFormat := GetEnvStringValue(evLoggingTimeStampFormat, True);

      DbConnection.Protocol := GetEnvStringValue(evDbConnectionProtocol);
      DbConnection.HostName := GetEnvStringValue(evDbConnectionHostName);
      DbConnection.Port := GetEnvIntValue(evDbConnectionPort);
      DbConnection.User := GetEnvStringValue(evDbConnectionUser);
      DbConnection.Password := GetEnvStringValue(evDbConnectionPassword);
      DbConnection.Database := GetEnvStringValue(evDbConnectionDatabase);
      DbConnection.MaxConnections :=
        GetEnvIntValue(evDbConnectionMaxconnections, True, 2);
      DbConnection.Wait := GetEnvBoolValue(evDbConnectionWait, True, True);
      DbConnection.Timeout := GetEnvIntValue(evDbConnectionTimeout, True, 0);

      ApiCredencials.CommaText := GetEnvStringValue(evApiCredencials, False, 40000);
    finally
      UnlockAppSettings();
    end;
end;

function TAppSettings.LockAppSettngs: TSettings;
begin
  FLock.Enter();
  Result := FSettings;
end;

procedure TAppSettings.UnlockAppSettings;
begin
  FLock.Leave();
end;


//initialization
//  AppSettings := TAppSettings.Create();

//finalization
//  AppSettings.Free();

end.
